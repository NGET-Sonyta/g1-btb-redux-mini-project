import Axios from "axios";
import {
  EDIT_POST,
  GET_POST,
  DELETE_POST,
  GET_BY_CATE,
  FILTER_ARTICLE,
  GET_POST_BY_ID,
} from "./postActionTypes";

export const getPosts = () => {
  console.log("==>GET POST");

  return async (re) => {
    const result = await Axios.get("http://110.74.194.125:3535/api/articles");
    re({
      type: GET_POST,
      payLoad: result.data.data,
    });
  };
};

// export const getPostById = (id) => {
//   console.log("SEE ME??")
//   console.log(id)
//   return async (dispatch) => {
//     const result = await Axios.get(`http://110.74.194.125:3535/api/articles/${id}`)
//     dispatch({
//       type : GET_POST_BY_ID,
//       data: result.data.data,
//     })  
//   }
// }

export const updatePost = (id, article) => {
  console.log("update")
  console.log(id)
  return async(dispatch) => {
    const result = await Axios.patch(`http://110.74.194.125:3535/api/articles/${id}`, article)
    dispatch({
        type : EDIT_POST,
        data : result.data.data
    })
    console.log("update3")
  }
}

// export const deletePosts = () => {
export const deletePosts = (id) => {
  console.log("==>Delete");

  return async (del) => {
    const result = await Axios.delete(
      `http://110.74.194.125:3535/api/articles/${id}`
    );
    del({
      type: DELETE_POST,
      data: result,
    });
  };
};
export const updatePosts = (post, id, callBack) => {
  Axios.patch(`http://110.74.194.125:3535/api/articles/${id}`, post).then(
    (res) => {
      callBack(res.data.message);
    }
  );
};

export const getPostById = (id, callBack) => {
  Axios.get(`http://110.74.194.125:3535/api/articles/${id}`).then((res) => {
    callBack(res.data.data);
  });
};

export const postArticle = (post, callBack) => {
  Axios.post("http://110.74.194.125:3535/api/articles", post).then((res) => {
    callBack(res.data.message);
  });
};

export const getByCate = (cateID) => {
  return {
    type: GET_BY_CATE,
    payload: cateID,
  };
};
//postAction.js
export const filterArticle = (title) => {
  return (dp) => {
    Axios.get(`http://110.74.194.125:3535/api/articles?title=${title}`).then(
      (res) => {
        dp({
          type: FILTER_ARTICLE,
          payLoad: res.data.data,
        });
      }
    );
  };
};

// export const filterArticle = (title) => {
//   const innerSearch=async(dispatch)=>{
//       const result=await Axios.get(`http://110.74.194.125:3535/api/articles?title=${title}` )
//       dispatch({
//           type:FILTER_ARTICLE,
//           data:result.data,
//       })
//   }
//   return innerSearch
// };

//   return async (re) => {
//     const article = await Axios.get(
//       `http://110.74.194.125:3535/api/articles?title=${title}`
//     );
//     re({
//       type:FILTER_ARTICLE,
//       data: article.data.data,
//     });
//   };
// };



export const uploadImage = (file, callBack) => {
  Axios.post(`http://110.74.194.125:3535/api/images`, file).then((res) => {
    console.log(res.data.url);
    callBack(res.data.url);
  });
};

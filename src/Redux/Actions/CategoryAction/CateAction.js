import axios from "axios";
import { GET_CATE, POST_CATE, DELETE_CATE, EDIT_CATE } from "./CateActionType";

export const getCate = () => {
  return async (dp) => {
    axios.get("http://110.74.194.125:3535/api/category").then((res) => {
      dp({
        type: GET_CATE,
        payLoad: res.data.data,
      });
    });
  };
};

export const deleteCate = (id) => {
  console.log("==>Delete");

  return async (del) => {
    const result = await axios.delete(
      `http://110.74.194.125:3535/api/category/${id}`
    );
    del({
      type: DELETE_CATE,
      data: result,
    });
  };
};

export const editCate = (id, cate,ca) => {
  console.log("==>Edit");
  console.log("This is ", cate);
  console.log("This are", id);
  // return async (edit) => {
  //   const result = await axios.put(
  //     `http://110.74.194.125:3535/api/category/${id}`,cate
  //   );
  //   edit({
  //     type: EDIT_CATE,
  //     data: result,
  //   });
  // };
  axios.put(`http://110.74.194.125:3535/api/category/${id}`, cate).then((res) => {
    ca(res);
  });
};

// export const postCategory = (category) => {
//   return async (dp) => {
//     axios
//       .post("http://110.74.194.125:3535/api/category", category)
//       .then((res) => {
//         dp({
//           type: POST_CATE,
//           category: res.data,
//         });
//       });
//   };
// };

export const postCategory = (category) => {
  axios
    .post("http://110.74.194.125:3535/api/category", category)
    .then((res) => {
      console.log(res.data);
    });
};

import {
  EDIT_POST,
  GET_POST,
  DELETE_POST,
  GET_BY_CATE,
  FILTER_ARTICLE,
  GET_POST_BY_ID
} from "../../Actions/postAction/postActionTypes";

const defaultState = {
  data: [],
  findArticle: [],
  delete:{}
};

export const postReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_POST:
      return {
        ...state,
        data: [...action.payLoad],
      };
    case GET_BY_CATE:
      let filterCate = state.data.filter(
        (c) => (c.category ? c.category._id : null) === action.payload
      );
      return { ...state, findArticle: filterCate };
      //postReducer.js
    case FILTER_ARTICLE:
      return {
        ...state,
        data: action.payLoad,
      };
    case GET_POST_BY_ID:
      return {
        ...state,
        data: action.data,
      };
    case EDIT_POST:
      return {
        ...state,
        data: action.data,
      };
      case DELETE_POST:
        return{
          ...state,
          delete:action.data.data
        }
        case GET_POST_BY_ID:
          return {
            ...state,
            data: action.data,
          };
    default:
      return state;
  }
};

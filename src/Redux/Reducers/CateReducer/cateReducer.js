import {
  GET_CATE,
  POST_CATE,
  DELETE_CATE,
  EDIT_CATE,
} from "../../Actions/CategoryAction/CateActionType";

const defaultState = {
  category: [],
  delete: {},
  edit: {},
};

export const cateReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_CATE: {
      return {
        category: action.payLoad,
      };
    }
    case DELETE_CATE:
      return {
        ...state,
        delete: action.data.data,
      };
    case EDIT_CATE:
      return {
        ...state,
        edit: action.data.data,
      };
    default:
      return state;
  }
};

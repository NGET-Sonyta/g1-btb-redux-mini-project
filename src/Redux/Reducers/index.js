
import {postReducer} from './postReducer/postReducer'
import {cateReducer} from "./CateReducer/cateReducer"
import { combineReducers } from 'redux'


const reducer={
    postReducer: postReducer,
    cateReducer: cateReducer
}

export const rootReducer = combineReducers(reducer)
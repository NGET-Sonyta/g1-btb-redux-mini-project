import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { postReducer } from "./Redux/Reducers/postReducer/postReducer";
import thunk from "redux-thunk";
import { BrowserRouter } from "react-router-dom";

import { rootReducer } from "./Redux/Reducers";

import logger from "redux-logger/src";

// const store=createStore(postReducer, applyMiddleware(thunk))
// const store=createStore(rootReducer, applyMiddleware(thunk))
const middlewares = [thunk, logger];
const store = createStore(rootReducer, applyMiddleware(...middlewares));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

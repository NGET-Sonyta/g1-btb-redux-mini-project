import React, { Component } from "react";
import { Container, Button } from "react-bootstrap";
import { getPostById } from "../Redux/Actions/postAction/postActions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PImage from "./img/plainImage.png";
import strings from "./ConvertLang/strings";

class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      createDate: "",
      image: "",
      category: "",
    };
  }

  componentDidMount() {
    getPostById(this.props.match.params.id, (data) => {
      this.setState({
        title: data.title,
        description: data.description,
        createDate: data.updatedAt,
        image: data.image,
        category: data.category,
      });
    });
    console.log("Date is ", this.state.createDate);
  }
  render() {
    return (
      <div>
        <Container>
          <div>
            <div className="card mb-3">
              <div className="row no-gutters">
                <div className="col-md-4">
                  <img
                    // src={this.props.data.image ? this.state.image : PImage}
                    src={this.state.image ? this.state.image : PImage}
                    className="card-img"
                    alt="..."
                  />
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">{this.state.title}</h5>
                    <p className="card-text">{this.state.description}</p>
                    <p className="card-text">
                      <small className="text-muted">
                        {strings.createDate} : {this.state.createDate}
                      </small>
                    </p>
                    <p className="card-text">
                      <small className="text-muted">
                        {strings.category} :{" "}
                        {this.state.category
                          ? this.state.category.name
                          : "No Type"}{" "}
                      </small>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    data: state.postReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getPostById }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(View);

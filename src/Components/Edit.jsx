import React, { Component } from "react";
import { Form, Container,Image, Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import { getCate } from "../Redux/Actions/CategoryAction/CateAction";
import { getPostById, uploadImage, updatePost } from "../Redux/Actions/postAction/postActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PImage from "./img/plainImage.png";
import BigNavBar from "./BigNavBar";

class Edit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      imgFile: "", 
      imgLink: "http://110.74.194.125:3535/uploads/2020-06-26T14:14:34.489Z5ef2f26cd8751_1592980020_medium.png",
      categoryId: 0,
    };
  }
  componentDidMount() {
    this.props.getCate();
    // this.props.updatePost(this.props.match.params.id)
    console.log(this.props.data.image)
    console.log(this.state.imgLink)
  }

  componentWillMount(){
    this.props.getPostById(this.props.match.params.id)
    this.setState({
      imgLink: this.props.data.image
    })
    console.log(this.props.data.image)
    console.log(this.state.imgLink)
  }

  handleRadioButton = (event) => {
    if (event.target.checked) {
      this.setState(
        {
          category: event.target.value,
        },
        () => {
          console.log(this.state.category);
        }
      );
    }
  };

  handleTextChange = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state.title);
        console.log(this.state.description);
      }
    );
  };

  handleChangeImage = (e) => {
    this.setState({
      imgFile: e.target.files[0],
      imgLink: URL.createObjectURL(e.target.files[0]),
    });
  };

  updateArticle = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      uploadImage(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        console.log("With image", article.category);
        this.props.updatePost(this.props.match.params.id, article);
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
      };
      console.log("No image", article.categoryId);
      this.props.updatePost(this.props.match.params.id, article);
    }
  }

  handleUpdate = () =>{
    this.updateArticle()
  }

  render() {
    return (
      <div className="container" style={{ marginTop: "20px" }}>
        <div className="row">
          <div className="col-lg-8">
            <h1>Update Article</h1>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  placeholder="Input title"
                  defaultValue={this.props.data.title}
                  onChange={this.handleTextChange}
                />
              </Form.Group>
              <fieldset>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label as="legend" column sm={2}>
                    Category
                  </Form.Label>
                  <Col sm={10}>
                    {this.props.category.map((cate, index) => {
                      return (
                        <span key={index}>
                          <Form.Check
                            type="radio"
                            label={cate.name}
                            name="chkcategory"
                            value={cate._id}
                            id={cate.name}
                            // checked={this.state.category === cate.category}
                            onChange={this.handleRadioButton}
                          />
                        </span>
                      );
                    })}
                  </Col>
                </Form.Group>
              </fieldset>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  name="description"
                  defaultValue={this.props.data.description}
                  onChange={this.handleTextChange}
                  placeholder="Input Description"
                />
              </Form.Group>
            </Form>
            <Form.Group controlId="formBasicPassword">
              <Form.File
                id="custom-file"
                label="Custom file input"
                onChange={this.handleChangeImage}
                custom
              />
            </Form.Group>
          </div>
          <div className="col-lg-4">
            <Image
              src={this.state.imgLink ? this.state.imgLink : PImage}
              // src={this.props.data.image ? this.props.data.image : PImage}
              rounded
              className="w-100 p-3"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <Button
              variant="info"
              type="button"
              onClick={() => this.handleUpdate()}
            >
              Update
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    category: state.cateReducer.category,
    data: state.postReducer.data
  };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getCate, getPostById, updatePost}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);

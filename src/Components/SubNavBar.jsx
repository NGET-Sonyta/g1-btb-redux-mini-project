import React, { Component } from "react";
import { Navbar, Form, FormControl, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import { getCate } from "../Redux/Actions/CategoryAction/CateAction";
import {
  getByCate,
  filterArticle,
  getPosts,
  deletePosts
} from "../Redux/Actions/postAction/postActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ListPost } from "./ListPost";
import strings from "../Components/ConvertLang/strings"

class SubNavBar extends Component {
  componentWillMount() {
    this.props.getCate();
    this.props.getPosts();
  }
  componentDidUpdate(){
    this.props.getPosts();
  }
  handleDropdown = (e) => {
    this.props.getByCate(e.target.value);
  };
  render() {
    const styleA = { marginLeft: "10px" };
    const styleB = { marginLeft: "10px", height: "37px" };
    let article = [];
    // debugger;  

    if (this.props.findArticle.length === 0) {
      console.log("This first",this.props.data);
      article = this.props.data.map((art) => (
        <ListPost data={art} key={art._id}  deletePosts={this.props.deletePosts}/>
      ));
    } else {
      console.log("This second",this.props.data);
      article = this.props.findArticle.map((art) => (
        <ListPost data={art} key={art._id} deletePosts={this.props.deletePosts}/>
      ));
    }
    return (
      <div>
        <Navbar expand="lg">
          <Form inline style={{ marginLeft: "auto" }}>
            {strings.category}
            <Form.Control
              as="select"
              className="mr-sm-2"
              id="inlineFormCustomSelect"
              custom
              style={{ marginLeft: "10px" }}
              onChange={this.handleDropdown}
            >
              <option>All</option>

              {this.props.category
                ? this.props.category.map((cate, index) => {
                    {/* console.log("this is ", cate._id); */}
                    return (
                      <option key={index} value={cate._id}>
                        {cate.name}
                      </option>
                    );
                  })
                : null}
            </Form.Control>
            <FormControl
              type="text"
              style={styleA}
              placeholder="Search"
              className="mr-sm-2"
              onChange={(e) => this.props.filterArticle(e.target.value)}
            />
            <Button
              variant="info"
              style={styleA}
              as={Link}
              to={"/ArticleInput"}
            >
              {" "}
              {strings.add}
            </Button>
          </Form>
        </Navbar>
        {article}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    data: state.postReducer.data,
    category: state.cateReducer.category,
    findArticle: state.postReducer.findArticle,
  };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { getCate, getByCate, filterArticle, getPosts, deletePosts },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(SubNavBar);

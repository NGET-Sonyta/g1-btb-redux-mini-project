import React, { Component, useEffect } from "react";
import { Container, Row, Card, Button, Navbar } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import PImage from "./img/plainImage.png"
import strings from "./ConvertLang/strings"

export const ListPost = ({ data,deletePosts }) => {
  const covertDate = (dateStr) => {
    var date = dateStr.substring(0, 10);
    return date;
  };

  return (
    <div>
      <Container>
        <div key={data._id}>
          <div className="card mb-3">
            <div className="row no-gutters">
              <div className="col-md-4">
                <img src={data.image ? data.image : PImage} className="card-img" alt="..." />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">{data.title}</h5>
                  <p className="card-text">{data.description}</p>
                  <p className="card-text">
                    <small className="text-muted">
                      {strings.createDate} : {covertDate(data.createdAt)}
                    </small>
                  </p>
                  <p className="card-text">
                    <small className="text-muted">
                      {strings.category} :{" "}
                      {data.category ? data.category.name : "No Type"}{" "}
                    </small>
                  </p>
                  <Button variant="info" as={Link} to={`/View/id=${data._id}`}>
                    {strings.view}
                  </Button>{" "}
                  <Button variant="warning" as={Link} to={`/ArticleInput?isUpdate=true&&id=${data._id}`}>
                    {strings.edit}
                  </Button>{" "}
                  <Button variant="danger" onClick={()=>deletePosts(data._id)}>{strings.delete}</Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};

import React, { Component } from "react";
import { Form, Button, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getCate,
  postCategory,
  deleteCate,
  editCate,
} from "../Redux/Actions/CategoryAction/CateAction";
import { Table } from "react-bootstrap";
import strings from "./ConvertLang/strings";

class Category extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      isUpdate: false,
      updateID: "",
    };
  }
  componentDidMount() {
    this.props.getCate();
  }
  componentDidUpdate() {
    this.props.getCate();
  }
  handleAdd = () => {
    let category = {
      name: this.state.name,
    };
    if (this.state.isUpdate) {
      // editCate(this.state.updateID, category);
      // this.setState({
      //   isUpdate: false,
      //   name: "",
      // });
      editCate(this.state.updateID, category, (res) => {
        alert(res.data.message);
        this.setState({
          isUpdate: false,
          name: "",
        });
      });
    } else {
      postCategory(category, (res) => {
        // this.props.getCate();
        alert(res.data.message);
        this.setState({
          name: "",
        });
      });
    }
    // this.props.postCategory(category);
  };

  handleChange = (e) => {
    this.setState(
      {
        name: e.target.value,
      },
      () => {
        console.log(this.state.name);
      }
    );
  };
  handleEdit = (id, name) => {
    this.setState({
      name: name,
      isUpdate: true,
      updateID: id,
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <Form>
              <h3>{strings.category}</h3>
              <Form.Label>{strings.name}</Form.Label>
              <Form.Row>
                <Col xs={7}>
                  <Form.Control
                    type="text"
                    placeholder="Input category"
                    name="name"
                    onChange={this.handleChange}
                    value={this.state.name}
                  />
                </Col>
                <Col>
                  <Button variant="dark" type="button" onClick={this.handleAdd}>
                    {this.state.isUpdate ? strings.save : strings.addCategory}
                  </Button>
                </Col>
              </Form.Row>
            </Form>
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div className="col-lg-12">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>{strings.name}</th>
                  <th>{strings.action}</th>
                </tr>
              </thead>
              <tbody>
                {this.props.category.map((cate, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{cate.name}</td>
                      <td>
                        <Button
                          variant="primary"
                          onClick={() => this.handleEdit(cate._id, cate.name)}
                        >
                          {strings.edit}
                        </Button>
                        {"  "}
                        <Button
                          variant="danger"
                          onClick={() => this.props.deleteCate(cate._id)}
                        >
                          {strings.delete}
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    category: state.cateReducer.category,
  };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { getCate, postCategory, deleteCate, editCate },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Category);

import React, { Component, useEffect } from "react";
import { Form, Button, Row, Col, Image } from "react-bootstrap";
import PImage from "./img/plainImage.png";
import { getCate } from "../Redux/Actions/CategoryAction/CateAction";
import {
  postArticle,
  uploadImage,
  updatePosts,
  getPostById,
} from "../Redux/Actions/postAction/postActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import strings from "./ConvertLang/strings";
import queryString from "query-string";

class ArticleInput extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      categoryId: 0,
      description: "",
      imgFile: "",
      imgLink: "",
      isUpdate: false,
      updateId: "",
      titleError: "",
      descError: "",
    };
  }
  componentDidMount() {
    this.props.getCate();
    let path_data = queryString.parse(this.props.location.search);
    console.log("Path ", path_data);
    if (path_data.id !== undefined) {
      getPostById(path_data.id, (data) => {
        this.setState({
          updateId: path_data.id,
          title: data.title,
          description: data.description,
          categoryId: data.category ? data.category._id : null,
          isUpdate: true,
          imgLink: data.image,
        });
      });
    }
  }
  validation = () => {
    let titleError = "";
    let descError = "";
    if (!this.state.title) {
      titleError = "==> Please input title";
    }
    if (!this.state.description) {
      descError = "==> Please input description";
    }
    if (titleError || descError) {
      this.setState({
        titleError,
        descError,
      });
      return false;
    }
    return true;
  };
  handleRadioButton = (event) => {
    if (event.target.checked) {
      this.setState(
        {
          categoryId: event.target.value,
        },
        () => {
          console.log(this.state.categoryId);
        }
      );
    }
  };
  handleTextChange = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state.title);
        console.log(this.state.description);
      }
    );
  };
  handleUpdate = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      uploadImage(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        updatePosts(article, this.state.updateId, (message) => {
          alert(message);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
        image: this.state.imgLink,
      };
      updatePosts(article, this.state.updateId, (message) => {
        alert(message);
        this.props.history.push("/");
      });
    }
  };

  addArticle = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      uploadImage(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        console.log("With image", article.category);
        postArticle(article, (message) => {
          alert(message);
          this.props.history.push("/");
        });
        // postArticle(article);
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
      };
      console.log("No image", article.categoryId);
      postArticle(article, (message) => {
        alert(message);
        this.props.history.push("/");
      });
      // postArticle(article);
    }
  };

  handleChangeImage = (e) => {
    this.setState({
      imgFile: e.target.files[0],
      imgLink: URL.createObjectURL(e.target.files[0]),
    });
  };
  handleSubmit = () => {
    // alert(this.state.isUpdate)
    if (this.validation()) {
      this.state.isUpdate ? this.handleUpdate() : this.addArticle();
    }
    // this.addArticle();
  };
  render() {
    return (
      <div className="container" style={{ marginTop: "20px" }}>
        <div className="row">
          <div className="col-lg-8">
            <h1>{strings.article}</h1>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>{strings.title}</Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  placeholder="Input title"
                  value={this.state.title}
                  onChange={this.handleTextChange}
                />
                <Form.Text className="text-danger">
                  {this.state.titleError}
                </Form.Text>
              </Form.Group>
              <fieldset>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label as="legend" column sm={2}>
                    {strings.category}
                  </Form.Label>
                  <Col sm={10}>
                    {this.props.category.map((cate, index) => {
                      return (
                        <span key={index}>
                          <Form.Check
                            type="radio"
                            label={cate.name}
                            name="category"
                            value={cate._id}
                            id={cate.name}
                            checked={this.state.categoryId === cate._id}
                            onChange={this.handleRadioButton}
                          />
                        </span>
                      );
                    })}
                  </Col>
                </Form.Group>
              </fieldset>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>{strings.description}</Form.Label>
                <Form.Control
                  type="text"
                  name="description"
                  value={this.state.description}
                  onChange={this.handleTextChange}
                  placeholder="Input Description"
                />
                <Form.Text className="text-danger">
                  {this.state.descError}
                </Form.Text>
              </Form.Group>
            </Form>
            <Form.Group controlId="formBasicPassword">
              <Form.File
                id="custom-file"
                label="Custom file input"
                onChange={this.handleChangeImage}
                custom
              />
            </Form.Group>
          </div>
          <div className="col-lg-4">
            <Image
              src={this.state.imgLink ? this.state.imgLink : PImage}
              rounded
              className="w-100 p-3"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <Button
              variant="info"
              type="button"
              onClick={() => this.handleSubmit()}
            >
              {this.state.isUpdate ? strings.update : strings.submit}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state.cateReducer);
  return {
    category: state.cateReducer.category,
  };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getCate, postArticle, updatePosts }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleInput);

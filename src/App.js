import React, { Component } from "react";
import BigNavBar from "./Components/BigNavBar";
import NavBar from "./Components/SubNavBar";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import ArticleInput from "./Components/ArticleInput";
import View from "./Components/View";
import Category from "./Components/Category";
import SubNavBar from "./Components/SubNavBar";
import { ListPost } from "./Components/ListPost";
import strings from "./Components/ConvertLang/strings";

export default class App extends Component {
  onChangeLanguage = (str) => {
    strings.setLanguage(str);
    this.setState({});
  };
  render() {
    return (
      <div>
        <Router>
          <BigNavBar path="/" onChangeLanguage={this.onChangeLanguage} />
          <Switch>
            <Route
              path="/"
              exact
              render={() => (
                <SubNavBar onChangeLanguage={this.onChangeLanguage} />
              )}
            />
            <Route path="/ArticleInput" component={ArticleInput} />
            <Route
              path="/View/id=:id"
              exact
              // render={() => <View onChangeLanguage={this.onChangeLanguage} />}
              component={View}
            />
            <Route
              path="/Category"
              exact
              render={() => (
                <Category onChangeLanguage={this.onChangeLanguage} />
              )}
            />
            <Route
              path="/*"
              component={ListPost}
              onChangeLanguage={this.onChangeLanguage}
            />
          </Switch>
        </Router>
      </div>
    );
  }
}
